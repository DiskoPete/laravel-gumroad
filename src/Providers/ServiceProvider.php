<?php

namespace DiskoPete\LaravelGumroad\Providers;

use DiskoPete\LaravelGumroad\Services\Api;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/gumroad.php',
            'gumroad'
        );
    }

    public function register()
    {
        parent::register();

        $this->app->singleton(Api::class);

        $this->app->when(Api::class)
            ->needs('$accessToken')
            ->give(fn() => config('gumroad.accessToken'));
    }
}
