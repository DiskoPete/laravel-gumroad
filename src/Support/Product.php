<?php

namespace DiskoPete\LaravelGumroad\Support;

use Illuminate\Support\Fluent;

/**
 * @property string|null id
 * @property string|null name
 */
class Product extends Fluent
{
}
