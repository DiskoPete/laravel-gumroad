<?php

namespace DiskoPete\LaravelGumroad\Services;

use DiskoPete\LaravelGumroad\Support\Product;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

class Api
{
    public function __construct(
        private readonly string $accessToken
    )
    {
    }

    /**
     * @return Collection<int,Product>
     * @throws \Illuminate\Http\Client\RequestException
     */
    public function products(): Collection
    {
        return Http::get(
            "https://api.gumroad.com/v2/products",
            ['access_token' => $this->accessToken]
        )
            ->throw()
            ->collect('products')
            ->map(fn(array $data) => new Product($data));
    }


}
